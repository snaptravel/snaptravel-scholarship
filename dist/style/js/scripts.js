var clicked = false;

var dropdowncontent = document.querySelector('.dropdown-content')
var nav = document.querySelector('.nav')
var mobileHeader = document.querySelector('.mobile-header')

var aboutContainer = document.querySelector('.about-container')
var leftContainer = document.querySelector('.left-container')
var learnMoreContainer = document.querySelector('.learn-more-container2')
var stemContainer = document.querySelector('.stem-container')
var labels = document.querySelectorAll('.carousel__indicator')

document.querySelector('.dropbtn').addEventListener('click', function () {
    if (clicked) {
        dropdowncontent.style.display = "none";
        if (window.innerWidth < 1200 && window.scrollY < 150){
            mobileHeader.style.backgroundColor =  'unset'
        }
    }
    else {
        dropdowncontent.style.display = "block"
        mobileHeader.style.backgroundColor = "#f36518"
    };
    clicked = !clicked
});

window.addEventListener('scroll', function(e) {
    if (window.scrollY > 480){
        nav.classList.add('nav-sticky')
    }
    else{
        nav.classList.remove('nav-sticky')
    }
    if (window.innerWidth < 1200 && window.scrollY > 150){
        mobileHeader.style.position = 'fixed';
        mobileHeader.style.backgroundColor = "#f36518"
    }
    else if (window.innerWidth < 1200 && window.scrollY < 150){
        mobileHeader.style.position = 'unset';
        mobileHeader.style.backgroundColor =  'unset'
    }
    if (window.scrollY > learnMoreContainer.getBoundingClientRect().top - this.document.body.getBoundingClientRect().top - 100){
        labels[7].style.borderBottom = '3px solid #f36518'
        for (let i = 0; i < labels.length; i++){
            if (i !== 7) {
                labels[i].style.borderBottom = 'unset'
            }
        }
    }
    else if (window.scrollY > stemContainer.getBoundingClientRect().top - this.document.body.getBoundingClientRect().top - 100){
        labels[6].style.borderBottom = '3px solid #f36518'
        for (let i = 0; i < labels.length; i++){
            if (i !== 6) {
                labels[i].style.borderBottom = 'unset'
            }
        }
    }
    else if (window.scrollY > leftContainer.getBoundingClientRect().top - this.document.body.getBoundingClientRect().top - 100){
        labels[5].style.borderBottom = '3px solid #f36518'
        for (let i = 0; i < labels.length; i++){
            if (i !== 5) {
                labels[i].style.borderBottom = 'unset'
            }
        }
    }
    else if (window.scrollY > aboutContainer.getBoundingClientRect().top - this.document.body.getBoundingClientRect().top - 100){
        labels[4].style.borderBottom = '3px solid #f36518'
        for (let i = 0; i < labels.length; i++){
            if (i !== 4) {
                labels[i].style.borderBottom = 'unset'
            }
        }
    }

})

var about = function(){
    window.scrollTo({
        top:
          document.querySelector('.about').getBoundingClientRect().top -
          document.body.getBoundingClientRect().top - 70,
        behavior: 'smooth'
      })
}

var scholarship = function(){
    window.scrollTo({
        top:
          document.querySelector('.scholarship').getBoundingClientRect().top -
          document.body.getBoundingClientRect().top,
        behavior: 'smooth'
      })
}

var learnMore = function(){
    window.scrollTo({
        top:
          document.querySelector('.learn-more').getBoundingClientRect().top -
          document.body.getBoundingClientRect().top,
        behavior: 'smooth'
      })
}

var stem = function(){
    window.scrollTo({
        top:
          document.querySelector('.stem').getBoundingClientRect().top -
          document.body.getBoundingClientRect().top,
        behavior: 'smooth'
      })
}

